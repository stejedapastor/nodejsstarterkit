var express = require("express");

var app = express();
var port = 8090;

app.use('/', require('./api/routes/foo'));

app.listen(port, function() {
    console.log('Server started in http://localhost:' + port);
});
