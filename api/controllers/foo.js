var config = require('../../config/globalConfig');

function foo(req, res) {
	var fooJson = {
		message : config.foo.fooMessage
	};
    res.type('application/json');
    res.send(fooJson);
}

exports.foo = foo;
