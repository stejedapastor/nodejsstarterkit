var express = require('express');
var router = express.Router();
var fooController = require('../controllers/foo');

router.get('/foo', fooController.foo);

module.exports = router;
